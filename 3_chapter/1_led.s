.global _start /* 全局标号 */

/*
* 描述： _start 函数，程序从此函数开始执行此函数完成时钟使能、
* GPIO 初始化、最终控制 GPIO 输出低电平来点亮 LED 灯。
*/

_start:
/* 1、使能所有时钟 */
ldr r0, =0X020C4068 /* 寄存器 CCGR0 */
ldr r1, =0XFFFFFFFF 
str r1, [r0] //开启所得时钟，把0XFFFFFFFF写到r1里面
 
ldr r0, =0X020C406C /* 寄存器 CCGR1 */
str r1, [r0]
ldr r0, =0X020C4070 /* 寄存器 CCGR2 */
str r1, [r0]
ldr r0, =0X020C4074 /* 寄存器 CCGR3 */
str r1, [r0]
ldr r0, =0X020C4078 /* 寄存器 CCGR4 */
str r1, [r0]
ldr r0, =0X020C407C /* 寄存器 CCGR5 */
str r1, [r0]
ldr r0, =0X020C4080 /* 寄存器 CCGR6 */
str r1, [r0]

ldr r0, =0X020E0068//GPIO1_IO03的复用寄存器地址
ldr r1, =0X5//复用为GPIO模式
str r1, [r0]


/* 3、配置 GPIO1_IO03 的 IO 属性 
*bit 16:0 HYS 关闭
*bit [15:14]: 00 默认下拉
*bit [13]: 0 kepper 功能
*bit [12]: 1 pull/keeper 使能
*bit [11]: 0 关闭开路输出
*bit [7:6]: 10 速度 100Mhz
*bit [5:3]: 110 R0/6 驱动能力
*bit [0]: 0 低转换率
*/

ldr r0, =0X020E02F4/*寄存器 SW_PAD_GPIO1_IO03_BASE */
ldr r1, =0X10B0//上面所有到配置合起来就是这个数
str r1,[r0]

ldr r0, =0X0209C004 /*寄存器 GPIO1_GDIR */
ldr r1, =0X0000008
str r1,[r0]

ldr r0, =0X0209C000/*寄存器 GPIO1_DR */
ldr r1, =0//设置 GPIO1_IO03 输出低电平
str r1,[r0]

loop:
    b loop//经过指令b，不断的跳到loop函数执行，变成死循环