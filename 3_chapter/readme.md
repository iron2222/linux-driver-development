# 汇编语言点亮LED

拿到一款全新的芯片，第一个要做的事情的就是驱动其 GPIO，控制其 GPIO 输出高低电平。

GPIO口是IO口的一个功能之一。

## 一、接下来的步骤离不开芯片手册：

### 1.使能所有时钟，GPIO的所有时钟
### 2.IO配置，复用GPIO
### 3.设置IO寄存器
 配置IO 的上下拉、速度等。
### 4.配置GPIO
设置输入输出、默认输出高低电平，是否中断等。

## 二、在Linux虚拟机上进行交叉编译：

```
arm-linux-gnueabihf-gcc -g -c led.s -o led.o

arm-linux-gnueabihf-ld -Ttext 0X87800000 led.o -o led.elf

arm-linux-gnueabihf-objcopy -O binary -S -g led.elf led.bin

arm-linux-gnueabihf-objdump -D led.elf > led.dis
```
### 对这4步分别进行讲解：
第一步：把汇编文件.s编译成.o文件

第二步：进行连接，来将前面编译出来的 led.o 文件链接到 0X87800000 这个地址，会生成.elf文件

第三步：像一个格式转换工具，将 led.elf 文件转换为led.bin 文件

> 如果上电开始运行，没有OS系统：

> 如果将ELF格式的文件烧写进去，包含一些ELF文件的符号表字符表之类的section，运行碰到这些，就会导致失败，如果用objcopy生成纯粹的二进制文件，去除掉符号表之类的section，只将代码段数据段保留下来，程序就可以一步一步运行。

> elf文件里面包含了符号表等。BIN文件是将elf文件中的代码段，数据段，还有一些自定义的段抽取出来做成的一个内存的镜像。

第四步：反汇编

第五步：可以写一个makefile

## 三、SD卡下载，开发板运行
这个环节有两个要注意的点：

1.要了解SD卡烧录的过程，也就是别忘记imxdownload文件。

2.要找对，自己外插的SD卡设备是哪一个这个很重要。

```
ls /dev/sd*
```
插SD卡的时候注意，观察前后对比。

之后就是执行了

```
./imxdownload led.bin /dev/sdd
```
不同的SD卡设备是不一样的，要以自己的为准，比如我这个就是:

```
./imxdownload led.bin /dev/sdb
```
这个操作之后，会生成一个load.imx文件，实际上在SD卡里面的就是这个文件，和bin的区别就是在它前面加上了一些数据头。



## 源代码如下

```
1 
2 .global _start /* 全局标号 */
3 
4 /*
5 * 描述： _start 函数，程序从此函数开始执行此函数完成时钟使能、
6 * GPIO 初始化、最终控制 GPIO 输出低电平来点亮 LED 灯。
7 */
8 _start: 9 /* 例程代码 */
10 /* 1、使能所有时钟 */
11 ldr r0, =0X020C4068 /* 寄存器 CCGR0 */
12 ldr r1, =0XFFFFFFFF 
13 str r1, [r0] 
14 
15 ldr r0, =0X020C406C /* 寄存器 CCGR1 */
16 str r1, [r0]
17
18 ldr r0, =0X020C4070 /* 寄存器 CCGR2 */
19 str r1, [r0]
20 
21 ldr r0, =0X020C4074 /* 寄存器 CCGR3 */
22 str r1, [r0]
23 
24 ldr r0, =0X020C4078 /* 寄存器 CCGR4 */
25 str r1, [r0]
26 
27 ldr r0, =0X020C407C /* 寄存器 CCGR5 */
28 str r1, [r0]
29 
30 ldr r0, =0X020C4080 /* 寄存器 CCGR6 */
31 str r1, [r0]
32 
33
34 /* 2、设置 GPIO1_IO03 复用为 GPIO1_IO03 */
35 ldr r0, =0X020E0068 /* 将寄存器 SW_MUX_GPIO1_IO03_BASE 加载到 r0 中 */
36 ldr r1, =0X5 /* 设置寄存器 SW_MUX_GPIO1_IO03_BASE 的 MUX_MODE 为 5 */
37 str r1,[r0]
38
39 /* 3、配置 GPIO1_IO03 的 IO 属性 
40 *bit 16:0 HYS 关闭
41 *bit [15:14]: 00 默认下拉
42 *bit [13]: 0 kepper 功能
43 *bit [12]: 1 pull/keeper 使能
44 *bit [11]: 0 关闭开路输出
45 *bit [7:6]: 10 速度 100Mhz
46 *bit [5:3]: 110 R0/6 驱动能力
47 *bit [0]: 0 低转换率
48 */
49 ldr r0, =0X020E02F4 /*寄存器 SW_PAD_GPIO1_IO03_BASE */
50 ldr r1, =0X10B0
51 str r1,[r0]
52
53 /* 4、设置 GPIO1_IO03 为输出 */
54 ldr r0, =0X0209C004 /*寄存器 GPIO1_GDIR */
55 ldr r1, =0X0000008 
56 str r1,[r0]
57
58 /* 5、打开 LED0
59 * 设置 GPIO1_IO03 输出低电平
60 */
61 ldr r0, =0X0209C000 /*寄存器 GPIO1_DR */
62 ldr r1, =0 
63 str r1,[r0]
64
65 /*
66 * 描述： loop 死循环
67 */
68 loop:
69 b loop
```

