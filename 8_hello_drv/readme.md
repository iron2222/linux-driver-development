# 一个不牵涉硬件的字符驱动——HelloWorld！
## 写在前面：
> 这里会简单的告诉你一个驱动程序的框架，整个流程记住两个字就好——==借鉴==。人们接收知识最快的方式，不是看书思考，而是==观察模仿==（或者说实践也可以）。

## 功能要求：
- 驱动中可以实现open,write,read,release
- APP调用write时可以往驱动中写数据
- APP调用read时可以把驱动中保存的数据

 
### 1、确定主设备号
首先还是新建两个文件一个hello_drv.c、一个hello_drv_test.c。

#### hello_drv.c文件：
##### 头文件要包含哪些呢？

我也不知道，但我可以==借鉴==啊！

这里参考==Linux内核==里一个比较经典的==字符驱动程序misc.c==.


```
#include <linux/module.h>

#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/miscdevice.h>
#include <linux/kernel.h>
#include <linux/major.h>
#include <linux/mutex.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/stat.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/tty.h>
#include <linux/kmod.h>
#include <linux/gfp.h>
```
然后按步骤接着写。

确实主设备号

```
static int major = 0;
```



### 2、定义自己的file_operations结构体


```
/*2.定义自己的file_operations 结构体*/
static struct file_operations hello_drv = {
	.owner	 = THIS_MODULE,
	.open    = hello_drv_open,
	.read    = hello_drv_read,
	.write   = hello_drv_write,
	.release = hello_drv_close,

	};
```
在写的时候，时时刻刻记得==借鉴==。

等号右边都是函数，这里定义==一个结构体来包含驱动中的函数==。

### 3、实现对应的open/read/write等函数，填入file_operations结构体

四个驱动要包含的函数：
```
/*3.实现对应的open/read/write等函数，填入file_operations结构体*/
static ssize_t hello_drv_read (struct file *file, char __user *buf, size_t size, loff_t *offset)
{
	printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
	copy_to_user(buf, kernel_buf, MIN(1024, size));
	return MIN(1024, size);
}
static ssize_t hello_drv_write (struct file *file, char __user *buf, size_t size, loff_t *offset)
{
	printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
	copy_from_user(kernel_buf, buf, MIN(1024, size));
	return MIN(1024, size);

}
static int hello_drv_open (struct inode *node, struct file *file)
{
	printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
	return 0;
}
static int hello_drv_close (struct inode *node, struct file *file)
{
	printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
	return 0;

}
```

注意，要有缓冲区，来放数据。

```
static char kernel_buf[1024];
```
写入的时候，要用到：copy_from_user

```
copy_from_user(kernel_buf, buf, MIN(1024, size));
```
读的时候，要用到函数：copy_to_user

```
copy_to_user(buf, kernel_buf, MIN(1024, size));
```

而且读多少数据长度呢？这里直接定义一个，取最小值。

```
#define MIN(a,b) (s < b ? a : b)
```


### 4、把file_operations结构体告诉内核:注册驱动程序
现在已经有结构体了，下一步就是告诉内核，来注册一下，下一步，先要有一个入口函数。

### 5、谁来注册驱动程序?入口函数

```
/*5.谁来注册驱动程序啊?得有一个入口函数:安装驱动程序时，就会去调用这个入口函数*/
static int __init hello_init(void)
{
	
	printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
	major = register_chrdev(0,"hello",&hello_drv);
	
	hello_class = class_create(THIS_MODULE, "hello");//创造一个类
	err = PTR_ERR(hello_class);
	if (IS_ERR(hello_class))
		printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
		unregister_chrdev(0,"hello");
		return -1;

	device_create(adb_dev_class, NULL, MKDEV(major, 0), NULL, "hello");//创建设备
	
	return 0;
}
```

 
### 6、有入口函数就应该有出口函数:卸载驱动程序

```
/*6.有入口函数就应该有出口函数:卸载驱动程序时，就会去调用这个出口函数*/
static void  __exit hello_exit(void)
{
	printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
	device_destroy(hello_class, MKDEV(major, 0));//注销设备
	class_destroy(hello_class);//销毁类
	unregister_chrdev(0,"hello");
	return 0;
}
```


### 7、提供设备信息，自动创建设备节点

这里有两个宏，简单理解为，告诉计算机，哪个函数是入口函数，哪个是出口函数：

```
module_init(hello_init);
module_exit(hello_exit);
```
注意还有一个GPL协议：

```
MODULE_LICENSE("GPL");
```
> 如果没有这个协议，这个驱动是用不了的，Linux内核的使用前提，只要用了，就必须加上他，这也是它的传染性为什么这么大的原因。所以现在有些公司，驱动写的很简单，应用程序写的很复杂，就是为了规避这个协议，因为使用这个协议，就必须公布源码。



### 代码汇总一下
hello_drv.c
```
#include <linux/module.h>

#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/miscdevice.h>
#include <linux/kernel.h>
#include <linux/major.h>
#include <linux/mutex.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/stat.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/tty.h>
#include <linux/kmod.h>
#include <linux/gfp.h>


/*1.确定主设备号*/
static int major = 0;
static char kernel_buf[1024];
static struct class *hello_class;

#define MIN(a,b) (s < b ? a : b)

/*3.实现对应的open/read/write等函数，填入file_operations结构体*/
static ssize_t hello_drv_read (struct file *file, char __user *buf, size_t size, loff_t *offset)
{
	printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
	copy_to_user(buf, kernel_buf, MIN(1024, size));
	return MIN(1024, size);
}
static ssize_t hello_drv_write (struct file *file, char __user *buf, size_t size, loff_t *offset)
{
	printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
	copy_from_user(kernel_buf, buf, MIN(1024, size));
	return MIN(1024, size);

}
static int hello_drv_open (struct inode *node, struct file *file)
{
	printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
	return 0;
}
static int hello_drv_close (struct inode *node, struct file *file)
{
	printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
	return 0;

}

/*2.定义自己的file_operations 结构体*/
static struct file_operations hello_drv = {
	.owner	 = THIS_MODULE,
	.open    = hello_drv_open,
	.read    = hello_drv_read,
	.write   = hello_drv_write,
	.release = hello_drv_close,

	};

/*4.把 file _operations结构体告诉内核:注册驱动程序*/
/*5.谁来注册驱动程序啊?得有一个入口函数:安装驱动程序时，就会去调用这个入口函数*/
static int __init hello_init(void)
{
	
	printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
	major = register_chrdev(0,"hello",&hello_drv);
	
	hello_class = class_create(THIS_MODULE, "hello");
	err = PTR_ERR(hello_class);
	if (IS_ERR(hello_class))
		printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
		unregister_chrdev(0,"hello");
		return -1;

	device_create(adb_dev_class, NULL, MKDEV(major, 0), NULL, "hello");
	
	return 0;
}
/*6.有入口函数就应该有出口函数:卸载驱动程序时，就会去调用这个出口函数*/
static void  __exit hello_exit(void)
{
	printk("%s %s line %d\n", __FILE__, __FUNCTION__, __LINE__);
	device_destroy(hello_class, MKDEV(major, 0));
	class_destroy(hello_class);
	unregister_chrdev(0,"hello");
	return 0;
}

/*7.其他完善:提供设备信息，自动创建设备节点*/
module_init(hello_init);
module_exit(hello_exit);
MODULE_LICENSE("GPL");


```

至于hello_drv_test.c就很简单了，基本的读写文件操作，这里直接附上代码：

```

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unisted.h>
#include <stdio.h>
#include <string.h>

/*
 * ./hello_drv_tast -w abc
 * ./hello_drv_tast -r 
 */

int main(int argc, char * * argv)
{
	int fd;
	char buf[1024];
	int len;

	/*1、判断参数 */
	if(argc < 2)
	{
		printf("Usage: %s -w <string>\n",argv[0]);
		printf("	   %s -r\n",argv[0]);
		return -1;
	}

	/*2、打开文件 */
	fd = open("/dev/hello",O_RDWR);
	if(fd_old < -1)
	{
		printf("can not open file /dev/hello\n");
		return -1;
	}

	/*3、写文件或读文件*/
	if ((0 = strcmp(argv[1],"-w")) && (argc == 3))
	{
		len = strlen(argv[2]) + 1;
		len = len < 1024 ? len : 1024;
		write(fd,argv[2],len);
	}
	else
	{
		len = read(fd, buf, 1024);
		buf[1023] = '\0';
		printf("APP read : %s\n",buf);
	}

	close(fd);

	return 0;
}
```
## over！

> 不知怎么回事，群里突然说让去过上海迪士尼乐园的赶紧上报，疫情不会到上海了吧！害怕！大家一定要保护好自己，戴口罩，没事别乱跑！我要我藏得深，疫情就找不到我。

最后，祝大家早安、午安、晚安！