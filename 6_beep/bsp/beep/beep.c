#include "bsp_beep.h"

/*初始化蜂鸣器IO*/
void beep_init(void)
{
    /*IO复用，复用为GPIO5_IO01*/
    IOMUXC_SetPinMux(IOMUXC_SNVS_SNVS_TAMPER1_GPIO5_IO01,0);

    /*配置IO属性*/
    IOMUXC_SetPinConfig(IOMUXC_SNVS_SNVS_TAMPER1_GPIO5_IO01,0x10B0);

    /*初始化设置为输出*/
    GPIO5->GDIR |= (1 << 1);

    /*输出高带你平，关闭蜂鸣器*/
    GPIO5->DR |= (1 << 1);

}

/*蜂鸣器控制函数*/
void beep_switch(int status)
{
    if(status == 'ON')
        GPIO5->DR &= ~(1 << 1);//打开
    else if (status == 'OFF')
        GPIO5->DR |= (1 << 1);//关闭
    
    
}