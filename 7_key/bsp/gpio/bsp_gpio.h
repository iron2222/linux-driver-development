#ifndef _BSP_GPIO_h
#define _BSP_GPIO_h
#define _BSP_KEY_h
#include "imx6ul.h"

/*枚举类型和结构体定义*/
typedef enum _gpio_pin_direction
{
    kGPIO_DigitalInput = 0U,//输入，加一个U表示该常数是无符号整形
    kGPIO_DigitalOutput = 1U,//输入

}gpio_pin_direction_t;

/*GPIO配置结构体*/
typedef struct _gpio_pin_config
{
    gpio_pin_direction_t direction;//GPIO 方向：输入还是输出
    uint8_t outputLogic;//如果输出到话，默认输出电平
}gpio_pin_config_t;

/*函数声明*/
void gpio_init(GPIO_Type *base,int pin,gpio_pin_config_t *config);
int gpio_pinread(GPIO_Type *base,int pin);
void gpio_pinwrite(GPIO_Type *base,int pin,int value);

#endif // !_BSP_GPIO_h