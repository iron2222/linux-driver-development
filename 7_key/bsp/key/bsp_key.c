#include "bsp_key.h"
#include "bsp_gpio.h"
#include "bsp_delay.h"

/*初始化按键*/
void key_init(void)
{
    gpio_pin_config_t key_config;

    //IO复用，GPIO1_IO18
    IOMUXC_SetPinMux(IOMUXC_UART1_CTS_B_GPIO1_IO18,0);

    //配置IO属性
    IOMUXC_SetPinConfig(IOMUXC_UART1_CTS_B_GPIO1_IO18,0xF080);

    //GPIO1-18设置为输入
    key_config.direction = kGPIO_DigitalInput;
    gpio_init(GPIO1,18, &key_config);

}

/*获取按键值*/
int key_getvalue(void)
{
    int ret = 0;
    static unsigned char release = 1;//按键松开

    if((release==1)&&(gpio_pinread(GPIO1,18) == 0))
    {
        delay(10);//延时防抖
        release = 0;//标记按键按下
        if (gpio_pinread(GPIO1,18) == 0)
            ret = KEY0_VALUE;
        
    }
    else if (gpio_pinread(GPIO1,18) == 1) //KEY0未按下
    {
        ret = 0;
        release = 1;//标记按键释放
    }
    return ret;
}
