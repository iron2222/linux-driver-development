#include "main.h"

/*时钟使能函数*/
void clk_enable(void)
{
    CCM_CCGR0 = 0xffffffff;
    CCM_CCGR1 = 0xffffffff;
    CCM_CCGR2 = 0xffffffff;
    CCM_CCGR3 = 0xffffffff;
    CCM_CCGR4 = 0xffffffff;
    CCM_CCGR5 = 0xffffffff;
    CCM_CCGR6 = 0xffffffff;
    
}

/*初始化led对应到GPIO*/
void led_init(void)
{
    /*IO复用为GPIO1_IO03*/
    SW_MUX_GPIO1_IO03 = 0x5;

    /*配置GPIO1_IO03的相关属性*/
    /*
    *bit 16:0 HYS 关闭
    *bit [15:14]: 00 默认下拉
    *bit [13]: 0 kepper 功能
    *bit [12]: 1 pull/keeper 使能
    *bit [11]: 0 关闭开路输出
    *bit [7:6]: 10 速度 100Mhz
    *bit [5:3]: 110 R0/6 驱动能力
    *bit [0]: 0 低转换率
    */
   SW_PAD_GPIO1_IO03 = 0X10B0;//上面寄存器按配置要求输入相应数字后，就变成了0X10B0

   GPIO1_GDIR = 0X0000008;//初始化 GPIO, GPIO1_IO03 设置为输出

   GPIO1_DR = 0X0;//设置 GPIO1_IO03 输出低电平，打开 LED0
}

/*开灯函数*/
void led_on(void)
{
    GPIO1_DR &= ~(1<<3);//将 GPIO1_DR 的 bit3 清零
}

/*关灯函数*/
void led_off(void)
{
    GPIO1_DR |= (1<<3);// 将 GPIO1_DR 的 bit3 置 1
}


/*短时间延时函数*/
void short_delay(volatile unsigned int n)
{
    while(n--){}
}
/*延时函数 1ms*/
void delay(volatile unsigned int n)
{
    while(n--)
    {
        short_delay(0x7ff);
    }
}

/*主函数*/
int main()
{
    clk_enable();
    led_init();

    while (1)
    {
        led_off();
        delay(500);

        led_on();
        delay(500);

    }
    return 0;
}
