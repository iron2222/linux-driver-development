.global _start /* 全局标号 */

_start:

    /*进入SVC模式 */
    mrs r0, cpsr
    bic r0, r0, #0x1f/* 将 r0 的低 5 位清零，也就是 cpsr 的 M0~M4 */
    orr r0, r0, #0x13/* r0 或上 0x13,表示使用 SVC 模式 */
    msr cpsr, r0/* 将 r0 的数据写入到 cpsr_c 中 */

    ldr sp, =0X80200000/*设置栈指针 */

    /*因为 I.MX6U-ALPHA 开
    发 板 上 的 DDR3 地 址 范 围 是 0X80000000~0XA0000000(512MB) 或 者
    0X80000000~0X90000000(256MB)，不管是 512MB 版本还是 256MB 版本的，其 DDR3 起始地
    址都是 0X80000000。由于 Cortex-A7 的堆栈是向下增长的，所以将 SP 指针设置为 0X80200000，
    因此 SVC 模式的栈大小 0X80200000-0X80000000=0X200000=2MB，2MB 的栈空间已经很大了，
    如果做裸机开发的话绰绰有余。 */
    
    b main/*跳转到main函数 */