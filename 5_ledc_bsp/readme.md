# 一个大的工程文件是怎么建起来的？（BSP）
重点内容简介：
1. 工程目录框架？
1. 每个目录下要包含哪一类的文件？
1. 针对这种有好多个文件夹的工程，makefile该怎么写？


## 1、什么是BSP？
> 板级支持包（BSP）是介于主板硬件和操作系统中驱动层程序之间的一层，一般认为它属于操作系统一部分，主要是实现对操作系统的支持，为上层的驱动程序提供访问硬件设备寄存器的函数包，使之能够更好的运行于硬件主板。

BSP功能：
1. 单板硬件初始化，主要是CPU的初始化，为整个软件系统提供底层硬件支持
2. 为操作系统提供设备驱动程序和系统中断服务程序
3. 定制操作系统的功能，为软件系统提供一个实时多任务的运行环境
4. 初始化操作系统，为操作系统的正常运行做好准备。

是不是感觉就像boodloader一样？其实根据我查的资料来看，现在广义上的BSP，可以认为是：==bootlader+kernel+rootfs==。

纯粹的BSP所包含的内容一般说来是和系统==有关的驱动和程序==，如网络驱动和系统中网络协议有关，串口驱动和系统下载调试有关等等。

所以我这里举的一个例子，s是以裸机为依托，直接驱动led的，里面包含了：==激活CPU、配置运行环境、运行主程序==等文件。
## 2、一个简单的BSP框架
### 1）首先有一个imx6ul文件夹：
这个文件夹里会放一些头文件，这些头文件移植于官方SDK，里面是一些寄存器配置，还有官方提供的一些函数接口。

![image.png](https://note.youdao.com/yws/res/2/WEBRESOURCE203526d491d7a166565ac8606308fc42)

### 2）还有一个非标准函数库文件夹：
这个文件夹里还可以按照函数功能的不同再分成不同的目录。比如，延迟函数、时钟使能函数、led点亮函数等。

一般一个C文件，然后把里面的函数，封装成头文件来提供接口。

![image.png](https://note.youdao.com/yws/res/b/WEBRESOURCE41994da925bbacc4ad623e4197f23f6b)

### 3）当然还有项目文件夹：
里面有主函数，如果是裸机的话还可以包含一些汇编文件，来搭建好C环境。

![image.png](https://note.youdao.com/yws/res/d/WEBRESOURCE83c8791e521200ad4d8dc756bacaeacd)

### 4）以及最后的目标文件夹：
放编译之后的目标文件。

![image.png](https://note.youdao.com/yws/res/a/WEBRESOURCEb717170cb1fed36b364edfaa92b5426a)

### 5）还有主目录下的makefile以及连接脚本文件

![image.png](https://note.youdao.com/yws/res/3/WEBRESOURCE73d2300c7f427948516810bae1109393)

## 3、一个通用的makefile文件，怎么写？
这个内容量比较大，这里先放上代码，下次有时间，在接着说（导师来催干活了，考研的小伙伴一定不要选项目太多的老师啊！！！）

```
CROSS_COMPILE ?= arm-linux-gnueabihf-#这一行针对不同的编译器是可以进行更改的
TARGET		  ?= bsp#这个目标名字也是，针对不同到历程也是要改的

CC			  := $(CROSS_COMPILE)gcc
LD			  := $(CROSS_COMPILE)ld
OBJCOPY		  := $(CROSS_COMPILE)objcopy
OBJDUMP		  := $(CROSS_COMPILE)objdump
#变量 INCDIRS 包含整个工程的.h 头文件目录，文件中的所有头文件目录都要添加到变量INCDIRS中
INCDIRS		  := imx6ul \
				bsp/clk \
				bsp/led \
				bsp/delay
#SRCDIRS 包含的是整个工程的所有.c 和.S 文件目录
SRCDIRS 	  := project \
			  := bsp/clk \
			  := bsp/led \
			  := bsp/delay
#变量 INCLUDE 使用到了函数 patsubst，通过函数 patsubst 给变量 INCDIRS 添加一个“-I”，因为 Makefile 语法要求指明头文件目录的时候需要加上“-I”
INCLUDE		  := $(patsubst %, -I %, $(INCDIRS))

#变量 SFILES 保存工程中所有的.s 汇编文件(包含绝对路径)，变量 SRCDIRS 已经存放了工程中所有的.c 和.S 文件，所以我们只需要从里面挑出所有的.S 汇编文件即可
SFILES := $(foreach dir, $(SRCDIRS), $(wildcard $(dir)/*.S))

#变量 CFILES 和变量 SFILES 一样，只是 CFILES 保存工程中所有的.c 文件(包含绝对路径)
CFILES := $(foreach dir, $(SRCDIRS), $(wildcard $(dir)/*.c))

#使用函数 notdir 将 SFILES 和 CFILES 中的路径去掉
SFILENDIR := $(notdir $(SFILES))
CFILENDIR := $(notdir $(CFILES))

#默认所有的文件编译出来的.o 文件和源文件在同一个目录中
SOBJS := $(patsubst %, obj/%, $(SFILENDIR:.S=.o))
COBJS := $(patsubst %, obj/%, $(CFILENDIR:.c=.o))

#变量 OBJS 是变量 SOBJS 和 COBJS 的集合
OBJS := $(SOBJS) $(COBJS)

#VPATH 是指定搜索目录的，这里指定的搜素目录就是变量 SRCDIRS 所保存的目录，这样当编译的时候所需的.S 和.c 文件就会在 SRCDIRS 中指定的目录中查找
VPATH := $(SRCDIRS)

.PHONY: clean

$(TARGET).bin : $(OBJS)
	$(LD) -Timx6ul.lds -o $(TARGET).elf $^
	$(OBJCOPY) -O binary -S $(TARGET).elf $@
	$(OBJDUMP) -D -m arm $(TARGET).elf > $(TARGET).dis
	
$(SOBJS) : obj/%.o : %.S
	$(CC) -Wall -nostdlib -c -O2 $(INCLUDE) -o $@ $<


$(COBJS) : obj/%.o : %.c
	$(CC) -Wall -nostdlib -c -O2 $(INCLUDE) -o $@ $<

clean: 
	rm -rf $(TARGET).elf $(TARGET).dis $(TARGET).bin $(COBJS) $(SOBJS)

```
