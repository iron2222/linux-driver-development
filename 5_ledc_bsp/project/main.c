
#include "bsp_clk.h"
#include "bsp_delay.h"
#include "bsp_led.h"


int main(void)
{
	clk_enable();		/* 使能所有的时钟 			*/
	led_init();			/* 初始化led 			*/

	while(1)			/* 死循环 				*/
	{	
		led_switch(LED0,'ON');	/* 关闭LED 			*/
		delay(500);		/* 延时500ms 			*/

		led_switch(LED0,'OFF');		/* 打开LED 			*/
		delay(500);		/* 延时500ms 			*/
	}

	return 0;
}
